### Purpose

The goal of this project is to be able to serve a podcast feed and modify it at will. Any media file in the `./medias` directory will be served as an episode.

### Run

- install nvm (see https://github.com/nvm-sh/nvm) 
- install a recent version of node (v18 recommended): `nvm install v18.12.1`
- clone this repo
- `npm install`
- *optional* edit `settings.json` with the values you want
- create a `./medias` directory to host your episodes
- add media files to the `./medias` directory to create episodes
- `node server.js`

### Usage

#### Root

The root route will generate a podcast depending on the files present in the `./medias` directory. You can edit the `settings.json` to change the way the podcast is generated

#### itunes

The itunes route can be accessed by `http://yourserver:port/itunes`. The following queries can be added:

- q: [Required] the search query (example "star%20wars")
- c: [Optional] the country of the podcast (example: "fr")
- l: [Optional] the podcast number limit (example: 50 to get at most 50 entries)

Example: `http://yourserver:port/itunes?q=star%20wars&c=fr&l=50`

### Dependencies

The server uses [express](https://expressjs.com/). 

To generate the podcast feed, this project uses [node-podcast](https://github.com/maxnowack/node-podcast).

### Contributions

Any contribution is welcome. Please open Merge requests