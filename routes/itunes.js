import express from 'express';
const router = express.Router();
import { ItunesSearch, ItunesSearchOptions } from "node-itunes-search";
import { json2xml } from 'xml-js';
import { encode } from 'html-entities';



/**
 * Routing for the itunes queries
 */

router.get('/', (req, res) => {
    /**
     * request a podcast list with the followig possible queries:
     * q: [Required] the search query (example "star%20wars")
     * c: [Optional] the country of the podcast (example: "fr")
     * l: [Optional] the podcast number limit (example: 50 to get at most 50 entries)
     */
    let date = new Date()
    console.log(date.toLocaleDateString() + " - " + date.toLocaleTimeString() + " : " + "itunes called by " + req.ip + " with params " + JSON.stringify(req.query))
    let itunesQueries = {
        term: req.query.q,
        entity: "podcast"
    }
    if ('c' in req.query) itunesQueries.country = req.query.c.toUpperCase()
    if ('l' in req.query) itunesQueries.limit = req.query.l
    const searchOptions = new ItunesSearchOptions(itunesQueries);
    console.log(date.toLocaleDateString() + " - " + date.toLocaleTimeString() + " : " + "launching itunes query with " + JSON.stringify(itunesQueries))

    ItunesSearch.search(searchOptions).then((result) => {

        //generate an opml export
        if (req.query.f == "xml") {

            let opml = {
                _declaration: {
                    _attributes: {
                        version: '1.0',
                        encoding: 'UTF-8',
                        standalone: 'yes'
                    }
                },
                opml: {
                    _attributes: {
                        version: '1.0'
                    },
                    head: {
                        title: "Podcast Tester"
                    },
                    body: {
                        outline: {
                            _attributes: {
                                text: 'feeds'
                            },
                            outline: []
                        }
                    }
                }
            }

            // inject the results in the opml object
            result.results.forEach(podcast => {
                opml.opml.body.outline.outline.push({
                    _attributes: {
                        type: "rss",
                        text: encode(podcast.trackName),
                        xmlUrl: podcast.raw.feedUrl
                    }
                })
            })

            res.type('application/xml');
            res.send(json2xml(opml, { compact: true, spaces: 1 }).toString())

            //generate a simple json array export
        } else {
            let podcasts = Array()
            result.results.forEach(podcast => {
                podcasts.push(podcast.raw.feedUrl)
            })

            res.send(podcasts)
        }

    }).catch(e => {
        console.error(e);
        res.send("Error occurred: " + e)
    })

})

export { router as itunesRoutes }
